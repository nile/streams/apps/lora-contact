// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

package ch.cern.nile.app.generated;


import io.kaitai.struct.ByteBufferKaitaiStream;
import io.kaitai.struct.KaitaiStream;
import io.kaitai.struct.KaitaiStruct;

import java.io.IOException;
import java.util.ArrayList;

public class ContactPacket extends KaitaiStruct {
    public static ContactPacket fromFile(String fileName) throws IOException {
        return new ContactPacket(new ByteBufferKaitaiStream(fileName));
    }

    public ContactPacket(KaitaiStream _io) {
        this(_io, null, null);
    }

    public ContactPacket(KaitaiStream _io, KaitaiStruct _parent) {
        this(_io, _parent, null);
    }

    public ContactPacket(KaitaiStream _io, KaitaiStruct _parent, ContactPacket _root) {
        super(_io);
        this._parent = _parent;
        this._root = _root == null ? this : _root;
        _read();
    }
    private void _read() {
        this.seq = this._io.readU2le();
        this.myTagId = this._io.readU2le();
        this.encounters = new ArrayList<EncounterRecord>();
        {
            int i = 0;
            while (!this._io.isEof()) {
                this.encounters.add(new EncounterRecord(this._io, this, _root));
                i++;
            }
        }
    }
    public static class EncounterRecord extends KaitaiStruct {
        public static EncounterRecord fromFile(String fileName) throws IOException {
            return new EncounterRecord(new ByteBufferKaitaiStream(fileName));
        }

        public EncounterRecord(KaitaiStream _io) {
            this(_io, null, null);
        }

        public EncounterRecord(KaitaiStream _io, ContactPacket _parent) {
            this(_io, _parent, null);
        }

        public EncounterRecord(KaitaiStream _io, ContactPacket _parent, ContactPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.rfTagId = this._io.readU2le();
            this.numContacts = this._io.readU2le();
            this.startedAt = new PackedTime(this._io, this, _root);
        }
        private int rfTagId;
        private int numContacts;
        private PackedTime startedAt;
        private ContactPacket _root;
        private ContactPacket _parent;
        public int rfTagId() { return rfTagId; }
        public int numContacts() { return numContacts; }
        public PackedTime startedAt() { return startedAt; }
        public ContactPacket _root() { return _root; }
        public ContactPacket _parent() { return _parent; }
    }
    public static class PackedTime extends KaitaiStruct {
        public static PackedTime fromFile(String fileName) throws IOException {
            return new PackedTime(new ByteBufferKaitaiStream(fileName));
        }

        public PackedTime(KaitaiStream _io) {
            this(_io, null, null);
        }

        public PackedTime(KaitaiStream _io, EncounterRecord _parent) {
            this(_io, _parent, null);
        }

        public PackedTime(KaitaiStream _io, EncounterRecord _parent, ContactPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.rawTime = this._io.readU2le();
        }
        private Integer minute;
        public Integer minute() {
            if (this.minute != null)
                return this.minute;
            int _tmp = (int) ((rawTime() & 63));
            this.minute = _tmp;
            return this.minute;
        }
        private Integer hour;

        /**
         * In 24 hour format
         */
        public Integer hour() {
            if (this.hour != null)
                return this.hour;
            int _tmp = (int) (((rawTime() >> 6) & 31));
            this.hour = _tmp;
            return this.hour;
        }
        private Integer day;
        public Integer day() {
            if (this.day != null)
                return this.day;
            int _tmp = (int) (((rawTime() >> 11) & 31));
            this.day = _tmp;
            return this.day;
        }
        private int rawTime;
        private ContactPacket _root;
        private EncounterRecord _parent;

        /**
         * Had to result to operations as this bit-packed string uses non-consecutive bits being a short in little endian
         */
        public int rawTime() { return rawTime; }
        public ContactPacket _root() { return _root; }
        public EncounterRecord _parent() { return _parent; }
    }
    private int seq;
    private int myTagId;
    private ArrayList<EncounterRecord> encounters;
    private ContactPacket _root;
    private KaitaiStruct _parent;

    /**
     * tag internal sequence number to detect duplicates
     */
    public int seq() { return seq; }

    /**
     * Own tag id
     */
    public int myTagId() { return myTagId; }
    public ArrayList<EncounterRecord> encounters() { return encounters; }
    public ContactPacket _root() { return _root; }
    public KaitaiStruct _parent() { return _parent; }
}
