package ch.cern.nile.app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

import com.google.gson.JsonObject;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;

import ch.cern.nile.app.generated.ContactPacket;
import ch.cern.nile.common.json.JsonSerde;
import ch.cern.nile.common.streams.AbstractStream;
import ch.cern.nile.common.streams.StreamUtils;
import ch.cern.nile.common.streams.offsets.InjectOffsetProcessorSupplier;

import io.kaitai.struct.ByteBufferKaitaiStream;

// TODO(#NILE-1096): Refactor if decided to keep in the future
@SuppressWarnings({"checkstyle:IllegalThrows", "PMD.AvoidThrowingRawExceptionTypes", "PMD.AvoidDuplicateLiterals",
        "PMD.AvoidCatchingGenericException"})
public final class ContactStream extends AbstractStream {

    @Override
    public void createTopology(final StreamsBuilder builder) {
        builder
                .stream(getSourceTopic(), Consumed.with(Serdes.String(), new JsonSerde()))
                .filter(this::filterRecord)
                .processValues(new InjectOffsetProcessorSupplier(), InjectOffsetProcessorSupplier.getSTORE_NAME())
                .flatMapValues(this::mapValues)
                .filter(StreamUtils::filterEmpty)
                .to(getSinkTopic());
    }

    private boolean filterRecord(final String ignored, final JsonObject value) {
        return value != null && value.get("data") != null;
    }

    private List<Object> mapValues(final JsonObject value) {
        List<Object> loraEvents;
        try {
            loraEvents = createLoraEvents(value.get("data").getAsString());
            setLastReadOffset(value.get("offset").getAsLong());
        } catch (RuntimeException e) {
            logStreamsException(e);
            loraEvents = Collections.emptyList();
        }
        return loraEvents;
    }

    private List<Object> createLoraEvents(final String payload) {
        final ContactPacket contactPacket = decode(payload);
        final List<Object> encounters = contactPacket.encounters().stream().map(er -> {
            try {
                final String timezone = "Europe/Zurich";
                final Map<String, Object> records = new HashMap<>();
                final Instant instant = Instant.now();
                final int monthValue = instant.atZone(ZoneId.of(timezone)).getMonthValue();
                final int yearValue = instant.atZone(ZoneId.of(timezone)).getYear();
                final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm", java.util.Locale.ENGLISH);
                sdf.setTimeZone(TimeZone.getTimeZone(timezone));
                final Calendar calendar = Calendar.getInstance();
                calendar.setTimeZone(TimeZone.getTimeZone(timezone));
                // Month is the previous one
                // In case a message comes with the start day bigger than the current day,
                // we know this had to be the previous month
                // E.g message day = 23 current day = 22 => message occurred last month
                if (er.startedAt().day() > instant.atOffset(ZoneOffset.UTC).getDayOfMonth()) {
                    final Date parse = sdf.parse(
                            String.format("%s/%s/%sT%d:%d", er.startedAt().day(), monthValue, yearValue,
                                    er.startedAt().hour(),
                                    er.startedAt().minute()));
                    calendar.setTime(parse);
                    calendar.add(Calendar.MONTH, -1);
                } else { // Month is the current one
                    final Date parse = sdf.parse(
                            String.format("%s/%s/%sT%d:%d", er.startedAt().day(), monthValue, yearValue,
                                    er.startedAt().hour(),
                                    er.startedAt().minute()));
                    calendar.setTime(parse);
                }

                records.put("my_tag_id", contactPacket.myTagId());
                records.put("rf_tag_id", er.rfTagId());
                records.put("num_contacts", er.numContacts());
                records.put("timestamp", calendar.getTimeInMillis());
                return records;
            } catch (ParseException ex) {
                throw new RuntimeException(ex);
            }
        }).collect(Collectors.toList());
        return injectSchema(encounters);
    }

    private List<Object> injectSchema(final List<Object> message) {
        final Map<String, Object> schema = new HashMap<>();
        final Collection<Map<String, Object>> fields = new ArrayList<>();

        fields.add(Map.of("field", "rf_tag_id", "type", "int64", "optional", "false"));
        fields.add(Map.of("field", "my_tag_id", "type", "int64", "optional", "false"));
        fields.add(Map.of("field", "num_contacts", "type", "int64", "optional", "false"));
        fields.add(Map.of("field", "timestamp", "type", "int64", "optional", "false", "name",
                "org.apache.kafka.connect.data.Timestamp"));

        schema.put("type", "struct");
        schema.put("fields", fields);

        return message.stream().map(m -> {
            final Map<String, Object> obj = new HashMap<>();
            obj.put("schema", schema);
            obj.put("payload", m);
            return obj;
        }).collect(Collectors.toList());
    }

    private ContactPacket decode(final String payload) {
        final byte[] decodedBytes = Base64.getDecoder().decode(payload);
        return new ContactPacket(new ByteBufferKaitaiStream(decodedBytes));
    }

}
